Name:		libssh2
Version:	1.11.0
Release:	4
Summary:	A library implementing the SSH2 protocol
License:	BSD
URL:		https://www.libssh2.org/
Source0:	https://libssh2.org/download/libssh2-%{version}.tar.gz

Patch0:		backport-src-add-strict-KEX-to-fix-CVE-2023-48795-Terrapin-At.patch
Patch1:         libssh2-1.11.0-strict-modes.patch
Patch2:		backport-Add-NULL-pointer-check-for-outlen-before-use-1109.patch
Patch3:		backport-We-should-check-whether-key_method-is-a-NULL-pointer.patch
Patch4:		backport-Add-a-new-structure-to-separate-memory-read-and-file.patch
Patch5:		backport-Fix-an-out-of-bounds-read-in-_libssh2_kex_agree_inst.patch
Patch6:		backport-openssl-fix-cppcheck-found-NULL-dereferences-1304.patch
Patch7:		backport-userauth-avoid-oob-with-huge-interactive-kbd-respons.patch
Patch8:		backport-buildconf-drop.patch
Patch9:		backport-Prevent-possible-double-free-of-hostkey.patch
Patch10:	backport-Fix-unstable-connections-over-nonblocking-sockets.patch
Patch11:	backport-session-support-server-banners-up-to-8192-bytes-was-256.patch
 
BuildRequires:	coreutils findutils /usr/bin/man zlib-devel
BuildRequires:	gcc make sed openssl-devel > 1:1.0.2 openssh-server
BuildRequires:	glibc-langpack-en groff

%description
libssh2 is a library implementing the SSH2 protocol as defined by
Internet Drafts: SECSH-TRANS(22), SECSH-USERAUTH(25),
SECSH-CONNECTION(23), SECSH-ARCH(20), SECSH-FILEXFER(06)*,
SECSH-DHGEX(04), and SECSH-NUMBERS(10).

%package	devel
Summary:	Development files for libssh2
Requires:	%{name}%{?_isa} = %{version}-%{release} pkgconfig

%description	devel
The libssh2-devel package contains libraries and header files for
developing applications that use libssh2.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

sed -i s/4711/47%{?__isa_bits}/ tests/{openssh_fixture.c,test_ssh{2.c,d.test}}

%build
%configure --disable-silent-rules --enable-shared --disable-docker-tests
%make_build

%install
%make_install
%delete_la

make -C example clean
rm -rf example/.deps
find example/ -type f '(' -name '*.am' -o -name '*.in' ')' -delete

mv -v example example.%{_arch}

%check
echo "Running tests for %{_arch}"
if [ ! -c /dev/tty ]; then
	echo Skipping SSH test due to missing /dev/tty
	echo "exit 0" > tests/ssh2.sh
fi
%ifarch %{sparc} %{arm}
echo Skipping SSH test on sparc/arm
echo "exit 0" > tests/ssh2.sh
%endif
%ifarch ppc %{power64} aarch64
echo "Skipping mansyntax test on PPC* and aarch64"
echo "exit 0" > tests/mansyntax.sh
%endif
#LC_ALL=en_US.UTF-8 make -C tests check

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%{!?_licensedir:%global license %%doc}
%doc docs/AUTHORS NEWS README RELEASE-NOTES
%license COPYING
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%doc example.%{_arch}/
%{_includedir}/*.h
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/*.pc

%files help
%defattr(-,root,root)
%doc docs/BINDINGS.md docs/HACKING.md docs/TODO NEWS
%{_mandir}/man3/libssh2_*.3*

%changelog
* Tue Oct 29 2024 bitianyuan <bitianyuan@huawei.com> - 1.11.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport some upstream patches

* Tue Jun 04 2024 yueyuankun<yueyuankun@kylinos.cn> - 1.11.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add patch to work around strict permissions issues for sshd tests

* Tue Apr 16 2024 renmingshuai <renmingshuai@huawei.com> - 1.11.0-2
- Type:CVE
- ID:CVE-2023-48795
- SUG:NA
- DESC:fix CVE-2023-48795 and disable tests check

* Mon Aug 7 2023 renmingshuai <renmingshuai@huawei.com> - 1.11.0-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update to 1.11.0

* Tue Mar 28 2023 renmingshuai <renmingshuai@huawei.com> - 1.10.0-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport some upstream patches

* Thu Feb 16 2023 renmingshuai <renmingshuai@huawei.com> - 1.10.0-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Support rsa-sha2 agent flags

* Mon Oct 17 2022 zhangjun <zhangjun@kylinos.cn> - 1.10.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Prevent files from being skipped if the output buffer is too small

* Thu Sep 29 2022 zhangjun <zhangjun@kylinos.cn> - 1.10.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Avoid the inconsistent malloc return code for malloc(0)

* Sat Jul 30 2022 renmingshuai <renmingshuai@huawei.com> - 1.10.0-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:patched one upstream patch in 1.10.0-1 without changelog 

* Mon Mar 21 2022 yanglu <yanglu72@h-partners.com> - 1.10.0-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update libssh2 to 1.10.0

* Thu Sep 24 2020 yuboyun <yuboyun@huawei.com> - 1.9.0-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix heap-buffer-overflow in _libssh2_ntohu32

* Fri Sep 11 2020 gaihuiying <gaihuiying1@huawei.com> - 1.9.0-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix to use better bounds check

* Thu Jun 4 2020 songzifeng<songzifeng1@huawei.com> - 1.9.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix heap buffer overflow in kex.c

* Sat May 30 2020 songzifeng<songzifeng1@huawei.com> - 1.9.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix use of uninitialized value in transport.c

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.0-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC: add patches

* Sun Sep 15 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.9.0-1
- Package init
